﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class Projectile : MonoBehaviour
    {
        [Header("Parameters")]
        [SerializeField]
        private float projectileVelocity;
        public float ProjectileVelocity { get => projectileVelocity; protected set => projectileVelocity = value; }

        protected Tower parentTower;

        public void SetParentTower(Tower parentTower)
        {
            this.parentTower = parentTower;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Enemy hitEnemy = collision.collider.GetComponent<Enemy>();

            if (hitEnemy != null)
            {
                Destroy(hitEnemy.gameObject);
                Destroy(gameObject);
            }
            else if (collision.collider.gameObject != parentTower.gameObject)
            {
                Destroy(gameObject);
            }
        }
    }
}