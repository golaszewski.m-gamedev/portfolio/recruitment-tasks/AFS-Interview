﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    
    public abstract class Tower : MonoBehaviour
    {
        [Header("Core parameters")]
        [SerializeField]
        protected float towerRange;

        [SerializeField]
        protected Transform projectileSpawnpoint;

        protected IReadOnlyList<Enemy> enemies;

        protected Enemy targetEnemy;

        public virtual void Initialize(IReadOnlyList<Enemy> enemies)
        {
            this.enemies = enemies;
        }

        protected Enemy FindClosestEnemy()
        {
            Enemy closestEnemy = null;
            float closestDistance = float.MaxValue;

            foreach (Enemy enemy in enemies)
            {
                float distance = (enemy.transform.position - transform.position).magnitude;
                if (distance <= towerRange && distance <= closestDistance)
                {
                    closestEnemy = enemy;
                    closestDistance = distance;
                }
            }

            return closestEnemy;
        }


        protected virtual void Update()
        {
            targetEnemy = FindClosestEnemy();

            // update rotation
            if (targetEnemy != null)
            {
                Quaternion lookRotation = Quaternion.LookRotation(targetEnemy.transform.position - transform.position);
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, lookRotation.eulerAngles.y, transform.rotation.eulerAngles.z);
            }
        }
    }
}