﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AlternativeTower : Tower
    {
        [Header("Burst fire parameters")]
        [SerializeField]
        private float burstIntervals;
        [SerializeField]
        private float burstFirerate;
        [SerializeField]
        private float burstShotAmount;

        [SerializeField]
        private AimedArrow arrowPrefab;

        private Coroutine burstFireCoroutine;

        public override void Initialize(IReadOnlyList<Enemy> enemies)
        {
            base.Initialize(enemies);

            burstFireCoroutine = StartCoroutine(BurstShootingCoroutine());
        }

        private IEnumerator BurstShootingCoroutine()
        {
            while (true)
            {
                for (int i = 0; i < burstShotAmount; ++i)
                {
                    // if the shot is off-cooldown the tower waits for the enemy to approach
                    if (targetEnemy == null)
                    {
                        yield return new WaitWhile(() => targetEnemy == null);
                    }

                    ShootArrowAt(targetEnemy);

                    yield return new WaitForSeconds(burstFirerate);
                }

                yield return new WaitForSeconds(burstIntervals);
            }
        }

        private Vector3 aimPosition;
        private Vector3 gravityCounterVelocity;

        private void ShootArrowAt(Enemy targetEnemy)
        {
            AimedArrow shotBullet = Instantiate(arrowPrefab, projectileSpawnpoint.position, new Quaternion());
            shotBullet.SetParentTower(this);

            // calculate lookahead based on the current enemy behavor and arrow speed
            Vector3 enemyPosition = targetEnemy.transform.position;
            Vector3 enemyVelocity = targetEnemy.GetCurrentVelocity();
            float enemySpeed = targetEnemy.GetCurrentVelocity().magnitude;

            float distanceToTarget = (enemyPosition - shotBullet.transform.position).magnitude;

            float travelTime;

            // Solving quadratic equation for interception points
            // (mathematical solution used provided in the topmost repo folder)
            {
                float a = 1 - Mathf.Pow(arrowPrefab.ProjectileVelocity, 2) / Mathf.Pow(enemySpeed, 2);
                float b = -2f * distanceToTarget * Mathf.Cos(Vector3.Angle((enemyPosition - shotBullet.transform.position), enemyVelocity));
                float c = Mathf.Pow(distanceToTarget, 2);
                float determinant = Mathf.Pow(b, 2) - 4f * a * c;

                Debug.Log(a);
                Debug.Log(b);
                Debug.Log(c);
                Debug.Log(determinant);

                if (determinant > 0)
                {
                    float travelTime1 = (-b + Mathf.Sqrt(determinant)) / (2f * a) / enemySpeed;
                    float travelTime2 = (-b - Mathf.Sqrt(determinant)) / (2f * a) / enemySpeed;

                    if (travelTime1 < travelTime2 && travelTime1 > 0)
                    {
                        travelTime = travelTime1;
                    }
                    else
                    {
                        travelTime = travelTime2;
                    }

                    if (travelTime > 0)
                    {
                        aimPosition = enemyPosition + enemyVelocity * travelTime;
                    }
                    else
                    {
                        MockSolution();
                    }
                }
                else if (determinant == 0)
                {
                    travelTime = -b / (2f * a) / enemySpeed;

                    if (travelTime > 0)
                    {
                        aimPosition = enemyPosition + enemyVelocity * travelTime;
                    }
                    else
                    {
                        MockSolution();
                    }
                }
                else 
                {
                    // no real solutions
                    MockSolution();
                }

                /// Just shoot at current enemy position
                void MockSolution() 
                {
                    Debug.LogWarning("MOCK SOLUTION USED!");

                    aimPosition = enemyPosition;
                    travelTime = distanceToTarget / arrowPrefab.ProjectileVelocity;
                }
            }

            // Adding the starting velocity so that the gravitational pull will not knock the projectile of the trajectory
            gravityCounterVelocity = (Physics.gravity * travelTime) * -0.5f;

            shotBullet.GetComponent<Rigidbody>().velocity = shotBullet.ProjectileVelocity * (aimPosition - shotBullet.transform.position).normalized + gravityCounterVelocity;
        }


        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(projectileSpawnpoint.position, aimPosition);
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(projectileSpawnpoint.position, arrowPrefab.ProjectileVelocity * (aimPosition - projectileSpawnpoint.position).normalized + gravityCounterVelocity);
        }
    }
}