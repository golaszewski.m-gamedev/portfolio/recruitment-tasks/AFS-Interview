﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class AimedArrow : Projectile
    {
        private Rigidbody thisRigidbody;

        private void Awake()
        {
            thisRigidbody = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            //Align body with movement dir
            transform.rotation = Quaternion.LookRotation(thisRigidbody.velocity.normalized);
        }
    }
}