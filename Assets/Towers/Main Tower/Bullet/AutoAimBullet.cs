﻿namespace AFSInterview
{
    using UnityEngine;

    public class AutoAimBullet : Projectile
    {
        [SerializeField]
        private float speed;

        private GameObject targetObject;

        private Renderer bulletRenderer;
        private Vector3 currentDirection;

        private void Awake()
        {
            // failsafe in case the target gets destroyed before direction is calculated
            currentDirection = transform.forward;
        }

        // the projectile will be destoroyed if it exits the camera view
        // prevents presistance of projectiles that have near-horizontal trajectory
        private void OnBecameInvisible()
        {
            if (targetObject == null)
            {
                Destroy(gameObject);
            }
        }

        public void SetTarget(GameObject target)
        {
            targetObject = target;
        }

        private void Update()
        {
            // projectile updates movement direction if it can
            if (targetObject != null)
            {
                currentDirection = (targetObject.transform.position - transform.position).normalized;
            }

            // otherwise will move in the last remembered
            transform.position += currentDirection * speed * Time.deltaTime;
        }
    }
}