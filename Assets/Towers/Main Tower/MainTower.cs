﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class MainTower : Tower
    {
        [Header("Rapid fire parameters")]
        [SerializeField]
        private float firingRate;

        [SerializeField]
        protected AutoAimBullet bulletPrefab;
        private Coroutine firingCoroutine;

        public override void Initialize(IReadOnlyList<Enemy> enemies)
        {
            base.Initialize(enemies);

            firingCoroutine = StartCoroutine(FiringCoroutine());
        }

        private IEnumerator FiringCoroutine()
        {
            while (true)
            {
                // if the shot is off-cooldown the tower waits for the enemy to approach
                if (targetEnemy == null)
                {
                    yield return new WaitWhile(() => targetEnemy == null);
                }

                AutoAimBullet bullet = Instantiate(bulletPrefab, projectileSpawnpoint.position, Quaternion.identity);
                bullet.SetParentTower(this);
                bullet.SetTarget(targetEnemy.gameObject);
                
                yield return new WaitForSeconds(firingRate);
            }
        }
    }
}
