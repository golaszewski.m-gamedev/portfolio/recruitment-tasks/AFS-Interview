﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;
    using UnityEngine.InputSystem;

    public class GameplayManager : MonoBehaviour
    {
        [Header("Controls")]
        [SerializeField]
        private InputActionReference placeMainTower;
        [SerializeField]
        private InputActionReference placeAlternativeTower;

        [Header("Enemy")] 
        [SerializeField]
        private Enemy enemyPrefab;
        [Header("Towers")]
        [SerializeField]
        private MainTower mainTowerPrefab;
        [SerializeField]
        private AlternativeTower alternativeTowerPrefab;

        [Header("Settings")] 
        [SerializeField] 
        private Vector2 boundsMin;
        [SerializeField]
        private Vector2 boundsMax;
        [SerializeField]
        private float enemySpawnRate;

        [Header("UI")] 
        [SerializeField]
        private TextMeshProUGUI enemiesCountText;
        [SerializeField]
        private TextMeshProUGUI scoreText;
        
        private List<Enemy> enemies;
        private int score;

        // cached for the purpose of easy breaking if need be in the larger scope of the project
        private Coroutine enemySpawningCoroutine;

        private void Awake()
        {
            enemies = new List<Enemy>();
            placeMainTower.action.Enable();
            placeAlternativeTower.action.Enable();

            enemySpawningCoroutine = StartCoroutine(EnemySpawning());
        }

        private void OnEnable()
        {
            placeMainTower.action.performed += PlaceMainTower;
            placeAlternativeTower.action.performed += PlaceAlternativeTower;
        }

        private void OnDisable()
        {
            placeMainTower.action.performed -= PlaceMainTower;
            placeAlternativeTower.action.performed -= PlaceAlternativeTower;
        }

        private IEnumerator EnemySpawning()
        {
            while (true)
            {
                SpawnEnemy();
                UpdateScoreDisplay();

                yield return new WaitForSeconds(enemySpawnRate);
            }
        }

        private void UpdateScoreDisplay()
        {
            scoreText.text = "Score: " + score;
            enemiesCountText.text = "Enemies: " + enemies.Count;
        }

        private void SpawnEnemy()
        {
            Vector3 position = new Vector3(Random.Range(boundsMin.x, boundsMax.x), enemyPrefab.transform.position.y, Random.Range(boundsMin.y, boundsMax.y));
            
            Enemy enemy = Instantiate(enemyPrefab, position, Quaternion.identity);
            enemy.OnEnemyDied += EnemyDiedCallback;
            enemy.Initialize(boundsMin, boundsMax);

            enemies.Add(enemy);
        }

        private void EnemyDiedCallback(Enemy enemy)
        {
            enemies.Remove(enemy);
            score++;

            UpdateScoreDisplay();
        }

        private bool TryGetValidPlacementPosition(out Vector3 position)
        {
            Ray ray = Camera.main.ScreenPointToRay(Pointer.current.position.ReadValue());

            if (Physics.Raycast(ray, out RaycastHit hit, LayerMask.GetMask("Ground")))
            {
                position = hit.point;
                return true;
            }
            else
            {
                position = Vector3.zero;
                return false;
            }
        }

        private void PlaceMainTower(InputAction.CallbackContext context)
        {
            Vector3 spawnPosition;

            if (TryGetValidPlacementPosition(out spawnPosition))
            {
                spawnPosition.y = mainTowerPrefab.transform.position.y;

                MainTower tower = Instantiate(mainTowerPrefab, spawnPosition, Quaternion.identity);
                tower.Initialize(enemies);
            }
        }

        private void PlaceAlternativeTower(InputAction.CallbackContext context)
        {
            Vector3 spawnPosition;

            if (TryGetValidPlacementPosition(out spawnPosition))
            {
                spawnPosition.y = alternativeTowerPrefab.transform.position.y;

                AlternativeTower tower = Instantiate(alternativeTowerPrefab, spawnPosition, Quaternion.identity);
                tower.Initialize(enemies);
            }
        }
    }
}